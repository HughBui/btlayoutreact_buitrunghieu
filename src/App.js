import logo from "./logo.svg";
import "./App.css";
import BaiTapLayout from "./BaiTapLayoutReact/BaiTapLayout";

function App() {
  return (
    <div className="App">
      <BaiTapLayout />
    </div>
  );
}

export default App;
