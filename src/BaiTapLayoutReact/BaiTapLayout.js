import React, { Component } from "react";
import Banner from "./Banner";
import Footer from "./Footer";
import Header from "./Header";
import Item from "./Item";

export default class BaiTapLayout extends Component {
  render() {
    return (
      <div>
        <Header />
        <div className="container">
          <Banner />
          <div className="row ">
            <Item />
            <Item />
            <Item />
            <Item />
            <Item />
            <Item />
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
